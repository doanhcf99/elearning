﻿    window.onload = function () {
        $("#btn_update_answer").hide();
        $('#ExpDate').datepicker({
            format: 'dd/mm/yyyy',
            trigger: $('#expDate'),
            language: "vi-VN",
        });
        $("#uploadFile").hide();
        var now = new Date();
        var today = "Dv" + "-" + now.getMilliseconds() + "-" + now.getMinutes() + "-" + now.getHours() + "-" + now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
        $("#Code").val(today);
    };
    var answers = [];
    var count = 0;
    function check_emptyQuestion() {
        //if (CKEDITOR.instances["Content"].getData().length == 0) {
        //    $("#messageQuestion").html("(*) bạn chưa nhập nội dung câu hỏi");
        //    return false;
        //} else {
        //    $("#messageQuestion").html("");
        //}
        if (CKEDITOR.instances["Content"].getData().length == 0) {
            $.confirm({
                title: 'Error',
                content: 'Nội dung câu hỏi không được để trống!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: 'Tiếp tục',
                        btnClass: 'btn-red',
                        action: function () {
                        }
                    },
                    close: function () {
                    }
                }
            });
            return false;
        }
        return true;
    }
    function check_empty(){
        //if (CKEDITOR.instances["answer"].getData().length == 0) {
        //    $("#messageAnswer").html("(*) bạn chưa nhập câu trả lời");
        //    return false;
        //} else {
        //    $("#messageAnswer").html("");
        //}
        if (CKEDITOR.instances["answer"].getData().length == 0) {
            $.confirm({
                title: 'Error',
                content: 'Nội dung câu trả lời không được để trống!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: 'Tiếp tục',
                        btnClass: 'btn-red',
                        action: function () {
                        }
                    },
                    close: function () {
                    }
                }
            });
            return false;
        } 
       
    return true;
};
    function check_null() {
    if (answers.length == 0) {
        $.confirm({
            title: 'Error',
            content: 'Vui lòng nhập câu trả lời!',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });
        return false;
    }
    return true;
};
    $('#TypeAnswer').on('change', function () {
        if ($('#TypeAnswer :selected').text() == "Một đáp án") {
            load_table_radio();
        } else {
            load_table();
        }
    });
    function check_tl() {
        let d = $('input[name^="check_content"]:checked').length
        if (d > 0) {
            $("#messageAnswer").html("");
        } else {
            $("#messageAnswer").html("(*) vui lòng chọn câu trả lời đúng!");
            return false;
        }
        return true;
    }
    function load_table() {
        let html = "";
        answers.forEach((e, i) => {
            html += `<tr>
                            <td>${e.id}</td>
                            <td>
                               <div class="form-check">
                                        <input class="form-check-input stt" name="check_content" type="checkbox" id="check_content" data-id="${e.id}" onchange="updateStatus(${e.id})" />
                                        <label class="form-check-label">${e.content}</label>
                                </div>
                            </td>
                            <td>${e.description}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" style="margin-right:5px" onclick="edit_answer(${e.id})">Sửa</button>
                                    <button type="button" class="btn btn-danger" onclick="remove_answer(${e.id});">Xoá</button>
                                </div>
                            </td>
                        </tr>`;
        });
        $("#content_answer").html(html);
    }
    function load_table_radio() {
        let html = "";
        answers.forEach((e, i) => {
            html += `<tr>
                            <td>${e.id}</td>
                            <td>
                                <div class="form-check">
                                      <label class="form-check-label" for="radio1">
                                        <input type="radio" class="form-check-input stt" name="check_content" id="radio1" name="optradio" onchange="updateStatus(${e.id})">${e.content}
                                      </label>
                                </div>
                            </td>
                            <td>${e.description}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" style="margin-right:5px" onclick="edit_answer(${e.id})">Sửa</button>
                                    <button type="button" class="btn btn-danger" onclick="remove_answer(${e.id});">Xoá</button>
                                </div>
                            </td>
                        </tr>`;
        });
        $("#content_answer").html(html);
    }
    function insert_answer() {
        if (check_empty()) {
            count++;
            let answer = { id: count, content: CKEDITOR.instances["answer"].getData(), description: CKEDITOR.instances["explain"].getData(), IsTrue: false };
            answers.push(answer);
            if ($('#TypeAnswer :selected').text() == "Một đáp án") {
                load_table_radio();
            } else {
                load_table();
            }
            edit_content();
        }
    }
    function updateStatus(id) {
        let index = answers.findIndex((x) => x.id === id);
        answers[index].IsTrue = true;
    }
    function remove_answer(id) {
        answers = answers.filter((x) => x.id != id);
        console.log(answers);
        load_table();
    }
    function edit_content(answer = "", explain = "") {
        CKEDITOR.instances["answer"].setData(answer);
        CKEDITOR.instances["explain"].setData(explain);
    }
    function edit_answer(id) {
        let index = answers.findIndex((x) => x.id === id);
        edit_content(answers[index].content, answers[index].explain);
        $("#btn_update_answer").attr("data-id", id);
        $("#btn_insertanswer").hide();
        $("#btn_update_answer").show();
    }
    $("#btn_update_answer").click((e) => {
        let id = Number($("#btn_update_answer").attr("data-id"));
        let index = answers.findIndex((x) => x.id === id);
        answers[index].content = CKEDITOR.instances["answer"].getData();
        answers[index].explain = CKEDITOR.instances["explain"].getData();
        $("#btn_insertanswer").show();
        $("#btn_update_answer").hide();
        edit_content();
        load_table();
    });

    function insert_data() {
        //const question = $('#frm_question').serializeObject();
        let question = {
            Code: $("#Code").val(),
            CategoryId: $("#CategoryId").val(),
            UnitId: $("#UnitId").val(),
            SubjectId: $("#SubjectId").val(),
            LevelId: $("#LevelId").val(),
            TypeQuestionId: $("#TypeQuestionId").val(),
            Content: CKEDITOR.instances["Content"].getData(),
            CountView: $("#CountView").val(),
            Tua: $("#Tua").is(":checked"),
            FullScreen: $("#FullScreen").is(":checked"),
            UrlVideo: $("#UrlVideo").val(),
            ExpDate: $("#ExpDate").val(),
            NumberAnswerLine: $("#NumberAnswerLine").val(),
            TypeAnswer: $("#TypeAnswer :selected").text(),
        };
        let c = {
            Question: question,
            Answers: answers,
        };
        if (check_emptyQuestion() && check_null() && check_tl()) {
            $.ajax({
                type: "POST",
                url: "/Question/Insert",
                data: JSON.stringify(c),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.location.href = "https://localhost:44344//Question/Index";
                },
                error: function (errormessage) { },
            });  
        }
};

    function selectFile() {
        var filename = $("#customFile").val();
        var ext = filename.split('.').pop();

        if (filename !== null) {
            $("#uploadFile").show();
            $("#uploadFile").on("click", uploadFile);
        } else {
            $("#uploadFile").hide();
        }
    }
    function uploadFile() {
        var filename = $("#customFile").val();
        if (filename.split('.').pop() == "mp3" || filename.split('.').pop() == "mp4") {
            let file = $("#customFile")[0].files[0];
            let data = new FormData();
            data.append("file", file);
            $.ajax({
                type: "POST",
                url: "/File/Upload",
                data: data,
                processData: false, // tell jQuery not to process the data
                contentType: false, // tell jQuery not to set contentType
                success: function (result) {
                    $("#UrlVideo").val(result);
                },
                error: function (err) {
                    console.log(err);
                },
            });
        } else {
            //$("#messageFile").html("(*) Chỉ nhận file mp3 hoặc mp4!");
            $.confirm({
                title: 'Error',
                content: 'Chỉ cho phép upload định dạng file mp3,mp4',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: 'Tiếp tục',
                        btnClass: 'btn-red',
                        action: function () {
                        }
                    },
                    close: function () {
                    }
                }
            });   
        }

};
    //$("#frm_question").validate({
    //    rules: {
    //        Code: "required",
    //        CategoryId: "required",
    //        UnitId: "required",
    //        SubjectId: "required"
    //    },

    //    messages: {
    //        Code: "Mã ko được để trống!",
    //        CategoryId: "Vui lòng chọn danh mục!",
    //        UnitId: "Vui lòng chọn đơn vị!",
    //        SubjectId: "Vui lòng chọn môn thi!"
    //    },
    //    submitHandler: function (form) {
          
    //    },
    //});