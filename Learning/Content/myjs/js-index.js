﻿window.onload = function () {
    $('#StartDate').datepicker({
        format: 'mm/dd/yyyy',
        trigger: $('#date1'),
        language: 'vi-VN'
    });
    $('#EndDate').datepicker({
        format: 'mm/dd/yyyy',
        trigger: $('#date2'),
        language: "vi-VN",
    });
   
};

$('#StartDate').on('change', function () {
    $('#EndDate').val('').datepicker("refresh");
    var minDate = $('#StartDate');
    var start = minDate.datepicker('getDate');
    $('#EndDate').datepicker('setStartDate', start);
});

function callModel() {
    $("#modal-default").modal("show");
};
$("#call_model").click((e) => {
    callModel();
});
function getId() {
    let id = $("#TypeQuestion").val();
    window.location.href = "/Question/Insert?idType=" + id;
};
function deleteData(id) {
    $.confirm({
        title: 'Xoá câu hỏi',
        content: 'Bạn có muốn xoá không?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: 'Tiếp tục',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: "DELETE",
                        url: "/Question/Delete",
                        data: {
                            Id: id,
                        },
                        success: function (response) {
                            window.location.href = "https://localhost:44344//Question/Index";
                        },
                        error: function (errormessage) { },
                    });
                }
            },
            close: function () {
            }
        }
    });
};
function checkStatus(id, status) {
    $.ajax({
        type: "POST",
        url: "/Question/CheckStatus",
        data: {
            QuestionId: id,
            Status: status
        },
        success: function (response) {
            window.location.href = "https://localhost:44344//Question/Index";
        },
        error: function (errormessage) { },
    });
};
$('#txt_checkall').change(function () {
    $('.inputtablequestion').prop('checked', this.checked);
});
$('.inputtablequestion').change(function () {
    if ($('.inputtablequestion:checked').length == $('.inputtablequestion').length) {
        $('#txt_checkall').prop('checked', true);
    }
    else {
        $('#txt_checkall').prop('checked', false);
    }
});
$('#deleteAll').click(function () {
    var array = [];
    let arraryId = $('.inputtablequestion');
    for (var i = 0; arraryId[i]; ++i) {
        if (arraryId[i].checked) {
            array.push(arraryId[i].value);
        }
    }
    if (array.length > 0) {
        $.confirm({
            title: 'Xoá tất cả câu hỏi đã chọn!',
            content: 'Bạn có thật sự muốn xoá không?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            type: 'POST',
                            url: '/Question/DeleteAll',
                            data: { mang: array },
                            success: function () {
                                window.location.href = "https://localhost:44344//Question/Index";
                            }
                        });
                    }
                },
                close: function () {
                }
            }
        });
    } else {
        $.confirm({
            title: '',
            content: 'Bạn chưa chọn câu hỏi cần xoá',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });
    }
});
$('#check_status').click(function () {
    var mang_status = [];
    let objArrary = $('.inputtablequestion');
    for (var i = 0; objArrary[i]; ++i) {
        if (objArrary[i].checked) {
            mang_status.push(objArrary[i].value);
        }
    }
    if ((mang_status).length > 0) {
        $.confirm({
            title: 'Duyệt tất cả!',
            content: 'Bạn có muốn duyệt tất cả các câu hỏi không?',
            type: 'success',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-green',
                    action: function () {
                        $.ajax({
                            type: 'POST',
                            url: '/Question/StatusTrue',
                            data: { mang: mang_status },
                            success: function () {
                                window.location.href = "https://localhost:44344//Question/Index";
                            }
                        });
                    }
                },
                close: function () {
                }
            }
        });
    } else {
        $.confirm({
            title: '',
            content: 'Bạn chưa chọn câu hỏi cần  duyệt',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });
    }
});
$('#check_status1').click(function () {
    var mang_status1 = [];
    let arr = $('.inputtablequestion');
    for (var i = 0; arr[i]; ++i) {
        if (arr[i].checked) {
            mang_status1.push(arr[i].value);
        }
    }
    if ((mang_status1).length > 0) {
        $.confirm({
            title: 'Bỏ duyệt tất cả',
            content: 'Bạn có chắc chắn muốn bỏ duyệt?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            type: 'POST',
                            url: '/Question/StatusFalse',
                            data: { mang: mang_status1 },
                            success: function () {
                                window.location.href = "https://localhost:44344//Question/Index";
                            },
                            error: function () {
                                alert('Không có dữ liệu!!!')
                            }
                        });
                    }
                },
                close: function () {
                }
            }
        });
    } else {
        $.confirm({
            title: '',
            content: 'Bạn chưa chọn câu hỏi cần bỏ duyệt',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });
    }
});
function Export() {
    let id = $('#TypeQuestionId').val();
    if (id > 0) {
        window.location.href = "https://localhost:44344/Excel/ExportExcel?Id=" + id;
    }else {
        $.confirm({
            title: '',
            content: 'Bạn chưa chọn loại câu hỏi!',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });
    }
}
