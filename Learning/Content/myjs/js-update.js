﻿window.onload = function () {
    $("#btn_update").hide();
    $('#uploadFile').hide();
    if ($('#TypeAnswer :selected').text() == "Một đáp án") {
        load_radio();
    } else {
        load_table();
    };
    $('#ExpDate').datepicker({
        format: 'dd/mm/yyyy',
        trigger: $('#updateDate'),
        language: "vi-VN",
    });
};
function callModel() {
    $("#modal-default").modal("show");
}
$("#call_model").click((e) => {
    callModel();
});
function getId() {
    let id = $("#TypeQuestion").val();
    window.location.href = "/Question/Insert?idType=" + id;
};
function check_empty() {
    if (CKEDITOR.instances["updateContent"].getData().length == 0) {
        $("#messageAnswer").html("(*) bạn chưa nhập câu trả lời");
        return false;
    } else {
        $("#messageAnswer").html("");
    }
    return true;
};
function check_tl() {
    let d = $('input[name^="check_content"]:checked').length
    if (d > 0) {
        $("#messageAnswer").html("");
    } else {
        $("#messageAnswer").html("(*) vui lòng chọn câu trả lời đúng!");
        return false;
    }
    return true;

};
var count = 0;

$('#TypeAnswer').on('change', function () {
    if ($('#TypeAnswer :selected').text() == "Một đáp án") {
        load_radio();
    } else {
        load_table();
    }
});
function load_table() {
    let html = '';
    data.forEach((e, i) => {
        html += `<tr>
                    <td>${e.Id}</td>
                    <td>
                        <div class="form-check">
                                <input class="form-check-input" ${e.Istrue == true ? "checked" : ""} type="checkbox" name="check_content" id="check_content" data-id="${e.Id}" onchange="updateStatus(${e.Id})" />
                                ${e.Content}
                         </div>
                    </td>
                    <td>${e.Description}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" style="margin-right:5px" onclick="update_answers(${e.Id})">Sửa</button>
                            <button type="button" class="btn btn-danger" onclick="remove_data(${e.Id});">Xoá</button>
                        </div>
                    </td>
                </tr>`;
    });
    $("#content_answer").html(html);
}
function load_radio() {
    let html = "";
    data.forEach((e, i) => {
        html += `<tr>
                        <td>${e.Id}</td>
                        <td>
                            <div class="form-check">
                                  <label class="form-check-label" for="radio1">
                                    <input type="radio" ${e.Istrue == true ? "checked" : ""} class="form-check-input" name="check_content" id="radio1" name="optradio" onchange="updateStatus(${e.Id})">${e.Content}
                                  </label>
                            </div>
                        </td>
                        <td>${e.Description}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" style="margin-right:5px" onclick="update_answers(${e.Id})">Sửa</button>
                                <button type="button" class="btn btn-danger" onclick="remove_data(${e.Id});">Xoá</button>
                            </div>
                        </td>
                    </tr>`;
    });
    $("#content_answer").html(html);
}
function insert_answer(id) {
    if (check_empty()) {
        count++;
        let answer = { Id: count, Content: CKEDITOR.instances["updateContent"].getData(), Description: CKEDITOR.instances["updateExplain"].getData(), Istrue: false, QuestionId: id };
        data.push(answer);
        let cons = $('#TypeAnswer :selected').text();
        if (cons == "Một đáp án") {
            load_radio();
        } else {
            load_table();
        }
        update_content();
    }
}
function updateStatus(id) {
    let index = data.findIndex((x) => x.Id === id);
    let st = data[index].Istrue;
    if (data[index].Istrue == true) {
        data[index].Istrue = false;
    } else {
        data[index].Istrue = true;
    }

}
function remove_data(Id) {
    data = data.filter(x => x.Id != Id);
    load_table();
}
function update_content(answer = "", explain = "") {
    CKEDITOR.instances["updateContent"].setData(answer);
    CKEDITOR.instances["updateExplain"].setData(explain);
}
function update_answers(Id) {
    let index = data.findIndex((x) => x.Id === Id);
    update_content(data[index].Content, data[index].Description);
    $("#btn_update").attr("data-Id", Id);
    $("#btn_insert").hide();
    $("#btn_update").show();
}
$("#btn_update").click((e) => {
    let id = Number($("#btn_update").attr("data-Id"));
    let index = data.findIndex((x) => x.Id === id);
    data[index].Content = CKEDITOR.instances["updateContent"].getData();
    data[index].Description = CKEDITOR.instances["updateExplain"].getData();
    $("#btn_insert").show();
    $("#btn_update").hide();
    update_content();
    load_table();
});
function update_data() {
    let question = {
        Id: $('#qId').val(),
        Code: $("#Code").val(),
        CategoryId: $("#CategoryId").val(),
        UnitId: $("#UnitId").val(),
        SubjectId: $("#SubjectId").val(),
        LevelId: $("#LevelId").val(),
        TypeQuestionId: $('#TypeQuestionId').val(),
        Content: CKEDITOR.instances["Content"].getData(),
        CountView: $("#CountView").val(),
        Tua: $("#Tua").is(":checked"),
        FullScreen: $("#FullScreen").is(":checked"),
        UrlVideo: $("#UrlVideo").val(),
        ExpDate: $('#ExpDate').val(),
        NumberAnswerLine: $("#NumberAnswerLine").val(),
        TypeAnswer: $("#TypeAnswer :selected").text()
    };
    let q_update = {
        Question: question,
        Answers: data
    };
    $.ajax({
        type: "POST",
        url: "/Question/Update",
        data: JSON.stringify(q_update),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            window.location.href = 'https://localhost:44344//Question/Index';
        },
        error: function (errormessage) {

        },
    });
};
function selectFile() {
    var filename = $("#customFile").val();
    var ext = filename.split('.').pop();
    console.log(ext);
    if (filename !== null) {
        $("#uploadFile").show();
        $("#uploadFile").on("click", uploadFile);
    } else {
        $("#uploadFile").hide();
    }
}
function uploadFile() {
    var filename = $("#customFile").val();
    if (filename.split('.').pop() == "mp3" || filename.split('.').pop() == "mp4") {
        let file = $("#customFile")[0].files[0];
        let data = new FormData();
        data.append("file", file);
        $.ajax({
            type: "POST",
            url: "/File/Upload",
            data: data,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            success: function (result) {
                $("#UrlVideo").val(result);
            },
            error: function (err) {
                console.log(err);
            },
        });
    } else {
        //$("#messageFile").html("(*) Chỉ nhận file mp3 hoặc mp4!");
        $.confirm({
            title: 'Error',
            content: 'Chỉ cho phép upload định dạng file mp3,mp4',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Tiếp tục',
                    btnClass: 'btn-red',
                    action: function () {
                    }
                },
                close: function () {
                }
            }
        });   
    }
}
$("#frm_question").validate({
    rules: {
        Code: "required",
        CategoryId: "required",
        UnitId: "required",
        SubjectId: "required"
    },
    messages: {
        Code: "Mã ko được để trống!",
        CategoryId: "Vui lòng chọn danh mục!",
        UnitId: "Vui lòng chọn đơn vị!",
        SubjectId: "Vui lòng chọn môn thi!"

    },
    submitHandler: function (form) {
        if (check_tl()) {
            update_data();
        }
    },
});