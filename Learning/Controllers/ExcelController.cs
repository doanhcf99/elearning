﻿using Learning.Models.BusinessModel;
using Learning.Models.DataModel;
using OfficeOpenXml;
using System;
using System.Dynamic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Learning.Controllers
{
    public class ExcelController : Controller
    {
        Repository db;
        public ExcelController()
        {
            db = new Repository();
        }
        public void ExportExcel(int Id)
        {
            var data = db.GetListQuestion(Id);
            var name = data.FirstOrDefault().TypeQuestionName;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "IdQuestion";
            ws.Cells["B1"].Value = "Mã câu hỏi";
            ws.Cells["C1"].Value = "CategoryId";
            ws.Cells["D1"].Value = "UnitId";
            ws.Cells["E1"].Value = "SubjectId";
            ws.Cells["F1"].Value = "LevelId";
            ws.Cells["G1"].Value = "UserId";
            ws.Cells["H1"].Value = "TypeQuestionId";
            ws.Cells["I1"].Value = "Content";
            ws.Cells["J1"].Value = "Description";
            ws.Cells["K1"].Value = "CountView";
            ws.Cells["L1"].Value = "Tua";
            ws.Cells["M1"].Value = "FullScreen";
            ws.Cells["N1"].Value = "UrlVideo";
            ws.Cells["O1"].Value = "ExpDate";
            ws.Cells["P1"].Value = "NumberAnswerLine";
            ws.Cells["Q1"].Value = "TypeAnswer";
            ws.Cells["R1"].Value = "Status";
            int count = 2;
            foreach (var item in data)
            {
                ws.Cells[string.Format("A{0}", count)].Value = item.Id;
                ws.Cells[string.Format("B{0}", count)].Value = item.Code;
                ws.Cells[string.Format("C{0}", count)].Value = item.CategoryId;
                ws.Cells[string.Format("D{0}", count)].Value = item.UnitId;
                ws.Cells[string.Format("E{0}", count)].Value = item.SubjectId;
                ws.Cells[string.Format("F{0}", count)].Value = item.LevelId;
                ws.Cells[string.Format("G{0}", count)].Value = item.UserId;
                ws.Cells[string.Format("H{0}", count)].Value = item.TypeQuestionId;
                ws.Cells[string.Format("I{0}", count)].Value = item.Content;
                ws.Cells[string.Format("J{0}", count)].Value = item.Description;
                ws.Cells[string.Format("K{0}", count)].Value = item.CountView;
                ws.Cells[string.Format("L{0}", count)].Value = item.Tua;
                ws.Cells[string.Format("M{0}", count)].Value = item.FullScreen;
                ws.Cells[string.Format("N{0}", count)].Value = item.UrlVideo;
                ws.Cells[string.Format("0{0}", count)].Value = item.ExpDate;
                ws.Cells[string.Format("P{0}", count)].Value = item.NumberAnswerLine;
                ws.Cells[string.Format("Q{0}", count)].Value = item.TypeAnswer;
                ws.Cells[string.Format("R{0}", count)].Value = item.Status;
                count++;
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name + ".xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
        public ActionResult Import()
        {
            dynamic data = new ExpandoObject();
            data.categoriesExcel = db.GetCategory();
            data.unitExcel = db.GetUnit();
            data.subjectExcel = db.GetSubject();
            return View(data);
        }
        [HttpPost]
        public JsonResult ImportExcel(CustomExcel data)
        {
            string path = Server.MapPath("/Uploads");
            string fileName = Path.GetFileName(data.file.FileName);
            string pathFile = Path.Combine(Server.MapPath("~/Uploads"), fileName);
            data.file.SaveAs(pathFile);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(pathFile));
            //var currentSheet = package.Workbook.Worksheets;
            var ws = package.Workbook.Worksheets[1];
            for (int rw = 2; rw < ws.Dimension.End.Row; rw++)
            {
                var test = ws.Cells[rw, 1].Value.ToString();
            }
            //using (ZipArchive archive = new ZipArchive(data.file.InputStream))
            //{
            //    foreach (ZipArchiveEntry entry in archive.Entries)
            //    {
            //        if (entry.FullName.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase))
            //        {
            //            string destinationPath = Path.GetFullPath(Path.Combine(path, entry.FullName));
            //            if (destinationPath.StartsWith(path, StringComparison.Ordinal))
            //                entry.ExtractToFile(destinationPath);
            //            FileInfo pathNew = new FileInfo(destinationPath);
            //            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            //            using (var package = new ExcelPackage(pathNew))
            //            {
            //                var currentSheet = package.Workbook.Worksheets;
            //                var ws = currentSheet.First();
            //                for (int rw = 2; rw < ws.Dimension.End.Row; rw++)
            //                {
            //                    var test = ws.Cells[rw, 1].Value.ToString();
            //                }
            //                //int count = 1;
            //                //var currentSheet = package.Workbook.Worksheets;
            //                //var workSheet = currentSheet.First();
            //                //var noOfCol = workSheet.Dimension.End.Column;
            //                //var noOfRow = workSheet.Dimension.End.Row;
            //                //Question q = new Question();
            //                //for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
            //                //{
            //                //    if (workSheet.Cells[rowIterator, count].Value != null)
            //                //    {
            //                //        q.Content = workSheet.Cells[rowIterator, count].Value.ToString();
            //                //    }
            //                //}
            //            }

            //        }

            //    }
            //}

            //FileRepository fs = new FileRepository();
            //string path = @"~/Uploads";
            //if (!Directory.Exists(Server.MapPath(path)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(path));
            //    FileUpLoad.SetFolderPermission(path);
            //}
            //string fileName = Path.GetFileName(data.file.FileName);
            //string pathFile = Path.Combine(Server.MapPath(path),
            //fileName);
            //data.file.SaveAs(pathFile);
            ////fs.AddFile(pathFile);
            //Excel.Application xlApp = new Excel.Application();
            //Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(pathFile);
            //Excel._Worksheet xlWorksheet = xlWorkbook.ActiveSheet;
            //Excel.Range xlRange = xlWorksheet.UsedRange;
            ////data question trong file excel
            //Question q = new Question();
            //q.Id = Convert.ToInt32(((Excel.Range)xlRange.Cells[2, 1]).Text);
            //q.Code = ((Excel.Range)xlRange.Cells[2, 2]).Text;
            //q.CategoryId = Convert.ToInt32(data.CategoryExcel);
            //q.UnitId = Convert.ToInt32(data.UnitExcel);
            //q.SubjectId = Convert.ToInt32(data.SubjectExcel);
            //q.Content = ((Excel.Range)xlRange.Cells[2, 3]).Text;
            //q.UrlVideo = ((Excel.Range)xlRange.Cells[2, 4]).Text;
            //q.CountView = Convert.ToInt32(((Excel.Range)xlRange.Cells[2, 5]).Text);
            //q.LevelId = Convert.ToInt32(((Excel.Range)xlRange.Cells[2, 6]).Text);
            //q.ExpDate = Convert.ToDateTime(data.ExpDate);
            //q.NumberAnswerLine = Convert.ToInt32(((Excel.Range)xlRange.Cells[2, 7]).Text);
            //q.TypeAnswer = ((Excel.Range)xlRange.Cells[2, 8]).Text;
            return Json(pathFile);
        }
    }
}