﻿using Learning.Models.BusinessModel;
using Learning.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Learning.Controllers
{
    public class HomeController : Controller
    {
        Repository db;
        public HomeController()
        {
            db = new Repository();
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult  List_Category()
        {
            return Json(db.GetCategory(), JsonRequestBehavior.AllowGet); 
        }
        public ActionResult AddCategory()
        {
            return View();
        }
        
    }
}