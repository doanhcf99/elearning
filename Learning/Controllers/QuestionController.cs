﻿using Learning.Models;
using Learning.Models.BusinessModel;
using Learning.Models.DataModel;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
namespace Learning.Controllers
{
    public class QuestionController : Controller
    {
        Repository db;
        public QuestionController()
        {
            db = new Repository();
        }
        // GET: Question
        public ActionResult Index(int? CategoryId, int? SubjectId, int? UnitId, DateTime? StartDate, DateTime? EndDate, int? TypeQuestionId, int? Status = -1, int page = 1)
        {
            var pageCurr = Request["PageCurrent"];
            var pageSize = Request["PageSize"];
            var dataSearch = new Search()
            {
                CategoryId = Convert.ToInt32(CategoryId),
                SubjectId = Convert.ToInt32(SubjectId),
                UnitId = Convert.ToInt32(UnitId),
                TypeQuestionId = Convert.ToInt32(TypeQuestionId),
                StartDate = StartDate.HasValue == true ? StartDate : (DateTime?)null ,
                EndDate = StartDate.HasValue == true ? EndDate : (DateTime?)null,
                Status = Convert.ToInt32(Status),
            };
            
            PaginationCustom paginate = new PaginationCustom
            {
                PageCount = 0,
                PageCurrent = page,
                PageSize = pageSize != null && pageSize != "" ? Convert.ToInt32(pageSize) : 3
            };
            dynamic data = new ExpandoObject();
            data.keySearch = dataSearch;
            data.categories = db.GetCategory();
            data.unit = db.GetUnit();
            data.subject = db.GetSubject();
            data.typequestion = db.GetTypeQuestion();
            IEnumerable<FullQuestion> questions = db.GetQuestion(CategoryId, SubjectId, UnitId, StartDate, EndDate, TypeQuestionId, Status, paginate).AsEnumerable();
            data.Questions = questions;
            var totalPage = questions.Count();
            int countPage = (questions.Count() > 0 && questions.First().TotalRows / paginate.PageSize > 1) ? questions.First().TotalRows / paginate.PageSize : 1;
            paginate.PageCount = countPage;
            data.pagination = paginate;
            data.answer = db.GetAnswer();
            return View(data);
        }
        [System.Web.Mvc.HttpGet]
        public ActionResult Insert(int idType)
        {
            dynamic data = new ExpandoObject();
            //data.getId = idType;
            data.typequestion = db.getById(idType);
            data.categories = db.GetCategory();
            data.unit = db.GetUnit();
            data.subject = db.GetSubject();
            //data.typequestion = db.GetTypeQuestion();
            data.level = db.GetLevel();
            return View(data);
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult Insert(QuestionAnswer q)
        {
            return Json(db.InsertQuestion(q), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int Id)
        {
            db.DeleteQuestion(Id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        [System.Web.Http.HttpGet]
        public ActionResult UpdateQuestion(int QuestionId)
        {
            dynamic dataUpdate = new ExpandoObject();
            dataUpdate.question = db.getQuestionById(QuestionId);
            dataUpdate.answer = db.getAnswerById(QuestionId);
            dataUpdate.category = db.GetCategory();
            dataUpdate.unit = db.GetUnit();
            dataUpdate.subject = db.GetSubject();
            dataUpdate.level = db.GetLevel();
            dataUpdate.typequestion = db.GetTypeQuestion();
            return View(dataUpdate);
        }
        [System.Web.Http.HttpPost]
        public JsonResult Update(QuestionAnswer qUpdate)
        {
            var idQuestion = qUpdate.Question.Id;
            db.deleteAnswer(idQuestion);
            foreach (var item in qUpdate.Answers)
            {
                db.InsertAnswerUpdate(item);
            }
            db.updateQuestion(qUpdate.Question);
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckStatus(int QuestionId,int Status)
        {
            db.CheckStatus(QuestionId, Status);
            return Json("check thành công",JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteAll(string[] mang)
        {
            for (int i = 0; i < mang.Length; i++)
            {
                db.DeleteQuestion(Convert.ToInt32(mang[i]));
            }
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        public JsonResult StatusTrue(string[] mang)
        {
            for (int i = 0; i < mang.Length; i++)
            {
                db.CheckStatusTrue(Convert.ToInt32(mang[i]));
            }
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        public JsonResult StatusFalse(string[] mang)
        {
            for (int i = 0; i < mang.Length; i++)
            {
                db.CheckStatusFalse(Convert.ToInt32(mang[i]));
            }
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        
    }
}