﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Learning.Models.DataModel;
using Learning.Models.BusinessModel;

namespace Learning.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase file)
        {
            FileRepository fs = new FileRepository();
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = @"~/Uploads";
                    if (!Directory.Exists(Server.MapPath(path)))
                    {

                        Directory.CreateDirectory(Server.MapPath(path));
                        FileUpLoad.SetFolderPermission(path);
                    }
                    string ext = Path.GetExtension(file.FileName);
                    if (ext == ".mp3" || ext == ".mp4")
                    {
                        string fileName = Path.GetFileName(file.FileName);
                        string pathFile = Path.Combine(Server.MapPath(path),
                        fileName);
                        file.SaveAs(pathFile);
                        fs.AddFile(pathFile);

                        pathFile = $"~/Uploads/{fileName}";
                        return Json(pathFile);
                    }
                    return Json("Phair mp3, mp4");
                }
                catch (Exception ex)
                {
                    return Json("ERROR:" + ex.Message.ToString());
                }

            return Json("Upload Successfully");
            
        }
    }
}