﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models
{
    public class PaginationCustom
    {
        public int PageCount { get; set; }
        public int PageCurrent { get; set; }
        public int PageSize { get; set; }
    }
}