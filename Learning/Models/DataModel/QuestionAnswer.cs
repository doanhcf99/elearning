﻿using Learning.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.BusinessModel
{
    public class QuestionAnswer
    {
        public QuestionDto Question { get; set; }

        public List<AnswerDto> Answers { get; set; }
    }
}