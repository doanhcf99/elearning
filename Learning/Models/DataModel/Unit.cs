﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class Unit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}