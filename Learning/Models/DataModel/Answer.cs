﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class Answer
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public bool Istrue { get; set; }
        public int Order { get; set; }
        public int QuestionId { get; set; }
    }

    public class AnswerDto
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Description { get;set;}
        public bool Istrue { get; set; }
        public int QuestionId { get; set; }
    }
}
