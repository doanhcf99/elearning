﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class TypeQuestion
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}