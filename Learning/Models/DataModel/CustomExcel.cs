﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class CustomExcel
    {
        public int? CategoryExcel { get; set; }
        public int? UnitExcel { get; set; }
        public int? SubjectExcel { get; set; }
        public DateTime? ExpDate { get; set; }
        public HttpPostedFileBase file { get; set; }
    }
}