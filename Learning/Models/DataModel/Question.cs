﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class Question
    {
       
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int UnitId { get; set; }
        [Required]
        public int SubjectId { get; set; }
        [Required]
        public int LevelId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int TypeQuestionId { get; set; }
        [Required]
        public string Content { get; set; }
        public string Description { get; set; }
        public int CountView { get; set; }
        public bool Tua { get; set; }
        public bool FullScreen { get; set; }
        public string UrlVideo { get; set; }
        public DateTime ExpDate { get; set; }
        public int NumberAnswerLine { get; set; }
        public string TypeAnswer { get; set; }
        public bool Status { get; set; }
    }

    public class QuestionDto
    {
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int UnitId { get; set; }
        [Required]
        public int SubjectId { get; set; }
        [Required]
        public int LevelId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string Content { get; set; }
        public string Description { get; set; }
        public int CountView { get; set; }
        public bool Tua { get; set; }
        public bool FullScreen { get; set; }
        public string UrlVideo { get; set; }
        public DateTime ExpDate { get; set; }
        public int NumberAnswerLine { get; set; }
        public string TypeAnswer { get; set; }
        public bool Status { get; set; }
        [Required]
        public int TypeQuestionId { get; set; }
    }

    public class FullQuestion
    {
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int UnitId { get; set; }
        [Required]
        public int SubjectId { get; set; }
        [Required]
        public int LevelId { get; set; }
        [Required]
        public int UserId { get; set; }
        public int? TypeQuestionId { get; set; }
        [Required]
        public string Content { get; set; }
        public string Description { get; set; }
        public int CountView { get; set; }
        public bool Tua { get; set; }
        public bool FullScreen { get; set; }
        public string UrlVideo { get; set; }
        public DateTime ExpDate { get; set; }
        public int NumberAnswerLine { get; set; }
        public string TypeAnswer { get; set; }
        public bool Status { get;set;}
        public string CategoryName { get; set; }
        public string UnitName { get; set; }
        public string SubjectName { get; set; }
        public string LevelName { get; set; }
        public string TypeQuestionName { get; set; }
        public int TotalRows { get; set; }
    }
    
}