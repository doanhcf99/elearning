﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.DataModel
{
    public class Search
    {
        public int CategoryId { get; set; }
        public int UnitId { get; set; }
        public int SubjectId { get; set; }
        public int TypeQuestionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Status { get; set; }

    }
}