﻿using Learning.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Learning.Models.BusinessModel
{
    public class FileRepository
    {
        private SqlConnection conn;
        public FileRepository()
        {
            conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$");
        }
        public bool AddFile(string file)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("AddFile", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Path", file);
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

    }
}