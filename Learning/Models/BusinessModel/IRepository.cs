﻿using Learning.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Learning.Models.BusinessModel
{
    public interface IRepository 
    {
        List<Category> GetCategory();
        List<Level> GetLevel();
        List<UserE> GetUserE();
        List<Unit> GetUnit();
        List<Subject> GetSubject();
        List<FullQuestion> GetQuestion(int? CategoryId, int? SubjectId, int? UnitId, DateTime? StartDate, DateTime? EndDate, int? TypeQuestionId);
        List<TypeQuestion> GetTypeQuestion();
        List<Answer> GetAnswer();
        object InsertQuestion(QuestionAnswer q);
        bool InsertAnswer(AnswerDto answer);
        bool DeleteQuestion(int Id);
    }

}