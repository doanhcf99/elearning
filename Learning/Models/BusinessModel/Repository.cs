﻿using FastMember;
using Learning.Models.DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Learning.Models.BusinessModel
{
    public class Repository
    {
        private SqlConnection conn;
        public List<AnswerDto> GetAnswer()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<AnswerDto> lst = new List<AnswerDto>();
                SqlCommand cmd = new SqlCommand("GetAnswer", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new AnswerDto()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        Istrue = Convert.ToBoolean(rdr["IsTrue"]),
                        QuestionId = Convert.ToInt32(rdr["QuestionId"])
                    };
                    lst.Add(data);
                };

                return lst;
            }
        }
        public List<Category> GetCategory()
        {
            List<Category> lst = new List<Category>();
            //SqlConnection conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$");
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCategory", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new Category()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"])
                    };
                    lst.Add(data);

                }
                //conn.Close();
                return lst;
            }

        }

        public List<Level> GetLevel()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<Level> lst = new List<Level>();
                SqlCommand cmd = new SqlCommand("GetLevel", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new Level()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString()
                    };
                    lst.Add(data);
                }
                //conn.Close();
                return lst;
            }

        }

        public List<FullQuestion> GetQuestion(int? CategoryId, int? SubjectId, int? UnitId, DateTime? StartDate, DateTime? EndDate, int? TypeQuestionId, int? Status, PaginationCustom pagination)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<FullQuestion> lst = new List<FullQuestion>();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetData";
                if (CategoryId == null)
                {
                    cmd.Parameters.AddWithValue("@CategoryId", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@CategoryId", CategoryId);
                }
                if (EndDate == null)
                {
                    cmd.Parameters.AddWithValue("@EndDate", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@EndDate", EndDate);
                }
                if (StartDate == null)
                {
                    cmd.Parameters.AddWithValue("@StartDate", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@StartDate", StartDate);
                }
                if (SubjectId == null)
                {
                    cmd.Parameters.AddWithValue("@SubjectId", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@SubjectId", SubjectId);
                }
                if (TypeQuestionId == null)
                {
                    cmd.Parameters.AddWithValue("@TypeQuestionId", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@TypeQuestionId", TypeQuestionId);
                }
                if (UnitId == null)
                {
                    cmd.Parameters.AddWithValue("@UnitId", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@UnitId", UnitId);
                }
                if (Status == -1)
                {
                    cmd.Parameters.AddWithValue("@Status", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Status", Status /*Status*/);
                }
                //cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@pageNumber", pagination.PageCurrent);
                cmd.Parameters.AddWithValue("@pageSize", pagination.PageSize);

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new FullQuestion()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Code = rdr["Code"].ToString(),
                        CategoryId = Convert.ToInt32(rdr["CategoryId"]),
                        UnitId = Convert.ToInt32(rdr["CategoryId"]),
                        SubjectId = Convert.ToInt32(rdr["CategoryId"]),
                        LevelId = Convert.ToInt32(rdr["LevelId"]),
                        UserId = Convert.ToInt32(rdr["UserId"]),
                        TypeQuestionId = Convert.ToInt32(rdr["TypeQuestionId"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        CountView = Convert.ToInt32(rdr["CountView"]),
                        Tua = Convert.ToBoolean(rdr["Tua"]),
                        FullScreen = Convert.ToBoolean(rdr["FullScreen"]),
                        UrlVideo = rdr["UrlVideo"].ToString(),
                        ExpDate = Convert.ToDateTime(rdr["ExpDate"]),
                        NumberAnswerLine = Convert.ToInt32(rdr["NumberAnswerLine"]),
                        TypeAnswer = rdr["TypeAnswer"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"]),
                        CategoryName = rdr["CategoryName"].ToString(),
                        UnitName = rdr["UnitName"].ToString(),
                        SubjectName = rdr["SubjectName"].ToString(),
                        LevelName = rdr["LevelName"].ToString(),
                        TypeQuestionName = rdr["TypeQuestionName"].ToString(),
                        //SubjectName = rdr["SubjectName"].ToString(),
                        TotalRows = Convert.ToInt32(rdr["TotalRows"]),
                    };
                    lst.Add(data);
                }
                //conn.Close();
                return lst;
            }

        }

        public List<Subject> GetSubject()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<Subject> lst = new List<Subject>();
                SqlCommand cmd = new SqlCommand("GetSubject", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new Subject()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"])
                    };
                    lst.Add(data);
                }
                //conn.Close();
                return lst;
            }

        }

        public List<TypeQuestion> GetTypeQuestion()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<TypeQuestion> lst = new List<TypeQuestion>();
                SqlCommand cmd = new SqlCommand("GetTypeQuestion", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new TypeQuestion()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString()
                    };
                    lst.Add(data);
                }
                //conn.Close();
                return lst;
            }

        }

        public List<Unit> GetUnit()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<Unit> lst = new List<Unit>();
                SqlCommand cmd = new SqlCommand("GetUnit", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new Unit()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString(),
                        Value = Convert.ToInt32(rdr["Value"])
                    };
                    lst.Add(data);
                }

                return lst;
            }

        }

        public List<UserE> GetUserE()
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<UserE> lst = new List<UserE>();
                SqlCommand cmd = new SqlCommand("GetUser", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new UserE()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString(),
                        Sex = Convert.ToBoolean(rdr["Sex"])
                    };
                    lst.Add(data);
                }
                //conn.Close();
                return lst;
            }

        }
        public object InsertQuestion(QuestionAnswer data)
        {
            var q = data.Question;
            SqlCommand cmd = new SqlCommand("InsertQuestion", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = q.Code ?? "";
            cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = q.CategoryId;
            cmd.Parameters.Add("@UnitId", SqlDbType.Int).Value = q.UnitId;
            cmd.Parameters.Add("@SubjectId", SqlDbType.Int).Value = q.SubjectId;
            cmd.Parameters.Add("@LevelId", SqlDbType.Int).Value = q.LevelId;
            cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@TypeQuestionId", SqlDbType.Int).Value = q.TypeQuestionId;
            cmd.Parameters.Add("@Content", SqlDbType.NText).Value = q.Content ?? "";
            //cmd.Parameters.Add("@Description", SqlDbType.NText).Value = q.Description ?? "";
            cmd.Parameters.Add("@CountView", SqlDbType.Int).Value = q.CountView;
            cmd.Parameters.Add("@Tua", SqlDbType.Bit).Value = q.Tua;
            cmd.Parameters.Add("@FullScreen", SqlDbType.Bit).Value = q.FullScreen;
            cmd.Parameters.Add("@UrlVideo", SqlDbType.VarChar).Value = q.UrlVideo ?? "";
            cmd.Parameters.Add("@ExpDate", SqlDbType.Date).Value = Convert.ToDateTime(q.ExpDate);
            cmd.Parameters.Add("@NumberAnswerLine", SqlDbType.VarChar).Value = q.NumberAnswerLine;
            cmd.Parameters.Add("@TypeAnswer", SqlDbType.NVarChar).Value = q.TypeAnswer;
            cmd.Parameters.Add("@Status", SqlDbType.Bit).Value = q.Status;
            cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                int QuestionId = Convert.ToInt32(cmd.Parameters["@Id"].Value);
                conn.Close();
                if (QuestionId > 0 && data.Answers.Count > 0)
                {
                    foreach (var a in data.Answers)
                    {
                        a.QuestionId = QuestionId;
                        this.InsertAnswer(a);
                    }
                }
                return true;
            }
            catch (Exception e)
            {

                return e.Message;
            }
        }
        public object InsertAnswer(AnswerDto a)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertAnswerDb", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Content", a.Content);
                    cmd.Parameters.AddWithValue("@Description", a.Description);
                    cmd.Parameters.AddWithValue("@IsTrue", a.Istrue);
                    //cmd.Parameters.AddWithValue("@Order", a.Order);
                    cmd.Parameters.AddWithValue("@Id", a.QuestionId);
                    cmd.ExecuteNonQuery();

                    return true;
                }
                catch (Exception e)
                {

                    return e.Message;
                }
            }

        }


        private object InsertAnswers(int QuestionId, List<AnswerDto> answers)
        {
            try
            {
                foreach (var a in answers)
                {
                    a.QuestionId = QuestionId;
                }
                DataTable table = new DataTable();
                using (var reader = ObjectReader.Create(answers.AsEnumerable(), "Content", "Description", "IsTrue", "Order", "QuestionId"))
                {
                    table.Load(reader);
                }
                SqlCommand cmd = new SqlCommand("InsertAnswers", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@listAnswer", table);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception e)
            {

                return e.Message;
            }
        }

        public bool DeleteQuestion(int Id)
        {

            try
            {
                SqlCommand cmd = new SqlCommand("DeleteQuestion", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", Id);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public TypeQuestion getById(int Id)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                SqlCommand cmd = new SqlCommand("GetTypeQuestionById", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", Id);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var q = new TypeQuestion()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Name = rdr["Name"].ToString()
                    };
                    return q;
                }
                return null;
            }
        }

        public QuestionDto getQuestionById(int QuestionId)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetQuestionById", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", QuestionId);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var question = new QuestionDto()

                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Code = rdr["Code"].ToString(),
                        CategoryId = Convert.ToInt32(rdr["CategoryId"]),
                        SubjectId = Convert.ToInt32(rdr["SubjectId"]),
                        UnitId = Convert.ToInt32(rdr["UnitId"]),
                        LevelId = Convert.ToInt32(rdr["LevelId"]),
                        UserId = Convert.ToInt32(rdr["UserId"]),
                        TypeQuestionId = Convert.ToInt32(rdr["TypeQuestionId"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        CountView = Convert.ToInt32(rdr["CountView"]),
                        Tua = Convert.ToBoolean(rdr["Tua"]),
                        FullScreen = Convert.ToBoolean(rdr["FullScreen"]),
                        UrlVideo = rdr["UrlVideo"].ToString(),
                        ExpDate = Convert.ToDateTime(rdr["ExpDate"]),
                        NumberAnswerLine = Convert.ToInt32(rdr["NumberAnswerLine"]),
                        TypeAnswer = rdr["TypeAnswer"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"])
                    };
                    return question;
                }
                return null;
            }
        }
        public List<AnswerDto> getAnswerById(int QuestionId)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                List<AnswerDto> list = new List<AnswerDto>();
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetAnswerById", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", QuestionId);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var answer = new AnswerDto()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        Istrue = Convert.ToBoolean(rdr["IsTrue"]),
                        QuestionId = Convert.ToInt32(rdr["QuestionId"])
                    };
                    list.Add(answer);
                }
                return list;
            }
        }
        public bool CheckStatus(int QuestionId, int Status)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("checkStatus", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", QuestionId);
                    cmd.Parameters.AddWithValue("@Status", Status);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        public object updateQuestion(QuestionDto qdto)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                var days = Convert.ToDateTime("1/1/0001");
                var now = DateTime.Now;
                var res = DateTime.Compare(days, qdto.ExpDate);
                if (res >= 0)
                {
                    qdto.ExpDate = now;
                }
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateQuestion", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", qdto.Id);
                    cmd.Parameters.AddWithValue("@Code", qdto.Code);
                    cmd.Parameters.AddWithValue("@CategoryId", qdto.CategoryId);
                    cmd.Parameters.AddWithValue("@UnitId", qdto.UnitId);
                    cmd.Parameters.AddWithValue("@SubjectId", qdto.SubjectId);
                    cmd.Parameters.AddWithValue("@LevelId", qdto.LevelId);
                    cmd.Parameters.AddWithValue("@UserId", 1);
                    cmd.Parameters.AddWithValue("@Content", qdto.Content);
                    cmd.Parameters.AddWithValue("@CountView", qdto.CountView);
                    cmd.Parameters.AddWithValue("@Tua", qdto.Tua);
                    cmd.Parameters.AddWithValue("@FullScreen", qdto.FullScreen);
                    cmd.Parameters.AddWithValue("@UrlVideo", qdto.UrlVideo);
                    cmd.Parameters.AddWithValue("@ExpDate", qdto.ExpDate);
                    cmd.Parameters.AddWithValue("@NumberAnswerLine", qdto.NumberAnswerLine);
                    cmd.Parameters.AddWithValue("@TypeAnswer", qdto.TypeAnswer);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
        public object updateAnswer(AnswerDto adto)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateAnswer", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", adto.QuestionId);
                    cmd.Parameters.AddWithValue("@Content", adto.Content);
                    cmd.Parameters.AddWithValue("@Description", adto.Description);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {

                    return e.Message;
                }

            }
        }
        public object deleteAnswer(int idQuestion)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteAnswer", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", idQuestion);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
        public object InsertAnswerUpdate(AnswerDto adto)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertAnswerDb", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Content", adto.Content);
                    cmd.Parameters.AddWithValue("@Description", adto.Description);
                    cmd.Parameters.AddWithValue("@IsTrue", adto.Istrue);
                    cmd.Parameters.AddWithValue("@Id", adto.QuestionId);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }
        public bool CheckStatusTrue(int QuestionId)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("checkStatus", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", QuestionId);
                    cmd.Parameters.AddWithValue("@Status", 0);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        public bool CheckStatusFalse(int QuestionId)
        {
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("checkStatus", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", QuestionId);
                    cmd.Parameters.AddWithValue("@Status", 1);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                }
            }
        }
        public IEnumerable<Question> ListQuestion()
        {
            List<Question> lst = new List<Question>();
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("ListQuestion", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new Question()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Code = rdr["Code"].ToString(),
                        CategoryId = Convert.ToInt32(rdr["CategoryId"]),
                        UnitId = Convert.ToInt32(rdr["CategoryId"]),
                        SubjectId = Convert.ToInt32(rdr["CategoryId"]),
                        LevelId = Convert.ToInt32(rdr["LevelId"]),
                        UserId = Convert.ToInt32(rdr["UserId"]),
                        TypeQuestionId = Convert.ToInt32(rdr["TypeQuestionId"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        CountView = Convert.ToInt32(rdr["CountView"]),
                        Tua = Convert.ToBoolean(rdr["Tua"]),
                        FullScreen = Convert.ToBoolean(rdr["FullScreen"]),
                        UrlVideo = rdr["UrlVideo"].ToString(),
                        ExpDate = Convert.ToDateTime(rdr["ExpDate"]),
                        NumberAnswerLine = Convert.ToInt32(rdr["NumberAnswerLine"]),
                        TypeAnswer = rdr["TypeAnswer"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"])
                    };
                    lst.Add(data);
                }
                return lst.AsEnumerable();
            }
        }
        public List<FullQuestion> GetListQuestion(int Id)
        {
            List<FullQuestion> lst = new List<FullQuestion>();
            using (conn = new SqlConnection(@"data source=DOANHDV\SQLEXPRESS01; Database=ELearning;uid=sa;pwd=1234$"))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("getQuestionId", conn);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var data = new FullQuestion()
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Code = rdr["Code"].ToString(),
                        CategoryId = Convert.ToInt32(rdr["CategoryId"]),
                        UnitId = Convert.ToInt32(rdr["CategoryId"]),
                        SubjectId = Convert.ToInt32(rdr["CategoryId"]),
                        LevelId = Convert.ToInt32(rdr["LevelId"]),
                        UserId = Convert.ToInt32(rdr["UserId"]),
                        TypeQuestionId = Convert.ToInt32(rdr["TypeQuestionId"]),
                        Content = rdr["Content"].ToString(),
                        Description = rdr["Description"].ToString(),
                        CountView = Convert.ToInt32(rdr["CountView"]),
                        Tua = Convert.ToBoolean(rdr["Tua"]),
                        FullScreen = Convert.ToBoolean(rdr["FullScreen"]),
                        UrlVideo = rdr["UrlVideo"].ToString(),
                        ExpDate = Convert.ToDateTime(rdr["ExpDate"]),
                        NumberAnswerLine = Convert.ToInt32(rdr["NumberAnswerLine"]),
                        TypeAnswer = rdr["TypeAnswer"].ToString(),
                        Status = Convert.ToBoolean(rdr["Status"]),
                        TypeQuestionName = rdr["TypeQuestionName"].ToString(),
                    };
                    lst.Add(data);
                }
                return lst;
            }
        }
        
    }
}