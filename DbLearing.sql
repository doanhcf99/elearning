﻿
CREATE DATABASE ELearning
GO
USE ELearning
GO
CREATE TABLE FileUpLoad
(
	Id int identity primary key,
	Path varchar(500) not null,
	Type varchar(50) default 'Question',
	ModelId int null
)
SELECT * FROM FileUpLoad
CREATE PROC AddFile
@Path varchar(500)
AS
BEGIN
	INSERT INTO FileUpLoad(Path) VALUES(@Path)
END
CREATE TABLE Category
(
	Id int identity(1,1) primary key ,
	Name nvarchar(50) not null,
	Status bit default 1
)
INSERT INTO Category(Name) VALUES(N'danh mục 1'),(N'danh mục 2'),(N'danh mục 3'),(N'danh mục 4'),(N'danh mục 5')
CREATE TABLE UserE
(
	Id int identity(1,1) primary key ,
	Name nvarchar(50) not null,
	Sex bit
)
INSERT INTO UserE(Name) VALUES(N'abc1'),(N'danh2'),(N'hanguyen1')
CREATE TABLE Unit
(
	Id int identity(1,1) primary key,
	Name nvarchar(50) not null,
	Value int
)
INSERT INTO Unit(Name) VALUES(N'đơn vị 1'),(N'đơn vị 2'),(N'đơn vị 3'),(N'đơn vị 4'),(N'đơn vị 5')
CREATE TABLE Subject
(
	Id int identity primary key,
	Name nvarchar(50) not null,
	Status bit default 1
)
INSERT INTO Subject(Name) VALUES(N'Toán'),(N'Lý'),(N'Hoá'),(N'Sinh'),(N'Sử')
CREATE TABLE Level
(
	Id int identity primary key,
	Name nvarchar(50) not null,
)
INSERT INTO Level(Name) VALUES(N'Level 1'),(N'Level 2'),(N'Level 3')
CREATE TABLE TypeQuestion
(
	Id int identity primary key,
	Name nvarchar(50) not null
)
INSERT INTO TypeQuestion(Name) VALUES(N'Trắc nghiệm'),(N'Câu Ghép'),(N'Câu Đơn')
CREATE TABLE Question
(
	Id int identity primary key,
	Code varchar(20) not null unique,
	CategoryId int not null,
	UnitId int not null,
	SubjectId int not null,
	LevelId int not null,
	Content ntext,
	Description ntext,
	CountView int,
	Tua bit,
	FullScreen bit,
	UrlVideo varchar(max),
	ExpDate Datetime,
	NumberAnswerLine int,
	TypeAnswer tinyint,
	UserId int not null,
	Status bit,
	FOREIGN KEY (CategoryId) REFERENCES Category(Id),
	FOREIGN KEY (UnitId) REFERENCES Unit(Id),
	FOREIGN KEY (SubjectId) REFERENCES Subject(Id),
	FOREIGN KEY (LevelId) REFERENCES Level(Id)
)
CREATE TABLE Answer
(
	Id int identity primary key,
	Content ntext,
	Description ntext,
	IsTrue bit,
	[Order] tinyint,
	QuestionId int,
	FOREIGN KEY (QuestionId) REFERENCES Question(Id)
)
ALTER TABLE Question
ADD FOREIGN KEY (UserId) REFERENCES UserE(Id);

CREATE PROC GetCategory
AS
BEGIN
	SELECT * FROM Category
END

CREATE PROC GetLevel
AS
BEGIN
	SELECT * FROM Level
END


CREATE PROC GetUser
AS
BEGIN
	SELECT * FROM UserE
END
exec GetUnit
CREATE PROC GetUnit
AS
BEGIN
	SELECT * FROM Unit
END

CREATE PROC GetSubject
AS
BEGIN
	SELECT * FROM Subject
END

CREATE PROC GetQuestion
AS
BEGIN
	SELECT * FROM Question
END

CREATE PROC GetTypeQuestion
AS
BEGIN
	SELECT * FROM TypeQuestion
END

CREATE PROC GetAnswer
AS
BEGIN
	SELECT * FROM Answer
END	

ALTER TABLE Question
ALTER COLUMN ExpDate Date;

create PROC InsertAnswers1
@Content ntext,
@Description ntext,
@IsTrue bit,
@QuestionId int
AS
BEGIN
	INSERT INTO Answer VALUES(@Content,@Description,@IsTrue,@QuestionId)
END

create proc InsertAnswers
	@listAnswer DtoAnswer READONLY 
as
begin
	insert into Answer(Content,Description,IsTrue,[Order],QuestionId)
	select * from @listAnswer
end


create type DtoAnswer as table (
	Content ntext,
	Description ntext,
	IsTrue bit,
	[Order] tinyint,
	QuestionId int
)
create PROC GetData
@CategoryId int = null,
@UnitId int = null,
@SubjectId int = null,
@StartDate Date = null,
@EndDate Date = null,
@TypeQuestionId int = null,
@Status bit = null,
@pageNumber int = 1,
@pageSize int = 10
AS
BEGIN 
	select 
	q.*,
	(select ct.Name from Category as ct where ct.Id = q.CategoryId) as CategoryName,
	(select ct.Name from Unit as ct where ct.Id = q.UnitId) as UnitName,
	(select ct.Name from Subject as ct where ct.Id = q.SubjectId) as SubjectName,
	(select ct.Name from Level as ct where ct.Id = q.LevelId) as LevelName,
	(select ct.Name from TypeQuestion as ct where ct.Id = q.TypeQuestionId) as TypeQuestionName,
	Count(*) Over() AS TotalRows
	from Question as q
	where
	(@CategoryId is null or q.CategoryId=@CategoryId)
	and
	(@UnitId is null or q.UnitId=@UnitId)
	and
	(@SubjectId is null or q.SubjectId=@SubjectId)
	and
	(@StartDate is null or q.ExpDate>=@StartDate)
	and
	(@EndDate is null or q.ExpDate<=@EndDate)
	and
	(@TypeQuestionId is null or q.TypeQuestionId=@TypeQuestionId)	
	and
	(@Status is null or q.Status=@Status)	
	order by id desc
	OFFSET (@pageNumber-1)*@pageSize ROWS
	FETCH NEXT @pageSize ROWS ONLY
END


ALTER TABLE Question
ADD TypeQuestionId int;

ALTER TABLE Question
ADD FOREIGN KEY (TypeQuestionId) REFERENCES TypeQuestion(Id);

SELECT * FROM Answer




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC InsertQuestion
@Code varchar(50),
@CategoryId int ,
@UnitId int ,
@SubjectId int,
@LevelId int,
@UserId int,
@TypeQuestionId int,
@Content ntext,
@CountView int,
@Tua bit,
@FullScreen bit,
@UrlVideo varchar(max),
@ExpDate Date = NULL,
@NumberAnswerLine int,
@TypeAnswer nvarchar(50),
@Status bit,
@Id int output
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO Question(Code,CategoryId,UnitId,SubjectId,LevelId,UserId,TypeQuestionId,Content,CountVIew,Tua,FullScreen,UrlVideo,ExpDate,NumberAnswerLine,TypeAnswer,Status) 
	VALUES (@Code,@CategoryId,@UnitId,@SubjectId,@LevelId,@UserId,@TypeQuestionId,@Content,@CountView,@Tua,@FullScreen,@UrlVideo,@ExpDate,@NumberAnswerLine,@TypeAnswer,@Status)
	SET @Id=SCOPE_IDENTITY()
	RETURN @Id
END

CREATE PROC DeleteQuestion
@Id int
as
begin
	DELETE FROM Answer WHERE QuestionId = @Id
	DELETE FROM Question WHERE Id = @Id
end



CREATE PROC GetTypeQuestionById
@Id int
as
begin
	select * from TypeQuestion where Id = @Id
end


CREATE PROC GetQuestionById
@Id int 
AS
BEGIN
	select * from Question where Id = @Id
END

exec GetQuestionById @id = 94
ALTER TABLE Question
ALTER  COLUMN Code varchar(20)


create proc getAnswerById
@Id int
as
begin
	select * from Answer where @Id = QuestionId
end

create proc checkStatus
@Id int,
@Status int
as
begin
	if(@Status = 0)
	begin 
	update Question set Status = 1 where @Id = Id
	end
	else
	begin 
	update Question set Status = 0 where @Id = Id
	end
end

create proc UpdateQuestion
@Id int,
@Code varchar(20),
@CategoryId int ,
@UnitId int ,
@SubjectId int,
@LevelId int ,
@UserId int,
@Content ntext,
@CountView int,
@Tua bit,
@FullScreen bit,
@UrlVideo varchar(max) = null,
@ExpDate Date,
@NumberAnswerLine int,
@TypeAnswer nvarchar(50)
as
begin
	update Question SET Code = @Code,CategoryId=@CategoryId,UnitId=@UnitId,SubjectId=@SubjectId,LevelId=@LevelId,UserId=@UserId,
		   Content=@Content,CountView=@CountView,FullScreen=@FullScreen,UrlVideo=@UrlVideo,ExpDate=@ExpDate,NumberAnswerLine=@NumberAnswerLine,
		   TypeAnswer=@TypeAnswer where Id = @Id
end

create proc UpdateAnswer
@Id int,
@Content ntext,
@Description ntext = null
as
begin
	update Answer set Content = @Content,Description =@Description where QuestionId = @Id
end

exec UpdateAnswer @Id  = 95 ,@Content = 'ansjdasda',@Description= '22131231'
select * from Answer
update Answer set Content = 'dadasdsa',Description ='123123' where QuestionId = 95

create proc DeleteAnswer
@Id int
as
begin
	DELETE FROM Answer where QuestionId = @Id
end

create proc InsertAnswerUpdate
@Id int,
@Content ntext,
@Description ntext = null,
@IsTrue bit
as
begin
	INSERT INTO Answer(Content,Description,QuestionId,IsTrue) VALUES(@Content,@Description,@Id,@IsTrue)
end



create proc InsertAnswerDb
@Id int,
@Content ntext,
@Description ntext = null,
@IsTrue bit
as
begin
	INSERT INTO Answer(Content,Description,QuestionId,IsTrue) VALUES(@Content,@Description,@Id,@IsTrue)
end


ALTER TABLE Question
ALTER COLUMN TypeAnswer nvarchar(50);